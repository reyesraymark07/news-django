from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Header
from .models import Author
from .models import Category as Categorie
from .models import Ad
from .models import Article

# Register your models here.

class NewsAdmin(admin.ModelAdmin):
    list_display = ['desc','name','action_time']
    list_per_page = 5
    search_fields = ['desc','name']
    list_display_links = ['desc','name']


class NewsAuthor(admin.ModelAdmin):
    list_display = ['desc','fullname','email','image_tag']
    search_fields = ['last_name','first_name']
    list_display_links = ['fullname']
    readonly_fields = ['image_tag']

class NewsCategory(admin.ModelAdmin):
    list_display = ['name','is_active','action_time','image_tag']
    readonly_fields = ['image_tag']
    search_fields = ['name']
    list_per_page = 1
    list_display_links = ['name']

class NewsBanner(admin.ModelAdmin):
    list_display = ['name','is_active','action_time','image_tag']
    search_fields = ['name']
    list_per_page = 5
    list_display_links = ['name']
    readonly_fields = ['image_tag']

class NewsArticle(admin.ModelAdmin):
    list_display = ['title']
    search_fields = ['title']
    list_per_page = 5
    list_display_links = ['title']

admin.site.register(Header,NewsAdmin)
admin.site.register(Author,NewsAuthor)
admin.site.register(Categorie,NewsCategory)
admin.site.register(Ad,NewsBanner)
admin.site.register(Article,NewsArticle)

