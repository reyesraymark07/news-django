from django.db import models
from django.utils.safestring import mark_safe
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
# Create your models here.
class Header(models.Model):
    name = models.CharField(max_length=100,default='')
    desc = models.TextField(default='')
    is_active = models.BooleanField(default=False)
    action_time = models.DateTimeField(auto_now=True)
     
    def __str__(self):
        return str(self.name) if self.name else ''

class Author(models.Model):
    first_name = models.CharField(max_length=100, null = True)
    last_name = models.CharField(max_length=100)
    image = models.ImageField(null=True,blank=True)
    desc = models.TextField()
    email = models.EmailField()
    is_active = models.BooleanField(default=False)
    date_register = models.DateTimeField(auto_now=True)

    @property
    def fullname(self):
        return '{} {}'.format(self.first_name, self.last_name)
    
    def image_tag(self):
        from django.utils.html import mark_safe
        return mark_safe('<img src="%s" width="100px" height="100px" />'%(self.image.url))
        image_tag.short_description = 'Image'
    
    def __str__(self):
         return str(self.fullname) if self.fullname else ''
       
class Category(models.Model):
    name = models.CharField(max_length=100, null = True)
    is_active = models.BooleanField(default=False)
    action_time = models.DateTimeField(auto_now=True)
    image = models.ImageField(null=True,blank=True)
    
    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    @property
    def image_tag(self):
        from django.utils.html import mark_safe
        return mark_safe('<img src="%s" width:"100px"  />'%(self.image.url))
        image_tag.short_description = 'Image'

    def __str__(self):
        return str(self.name) if self.name else ''

class Ad(models.Model):
    top = 'Top'
    side = 'Side'
    bottom = 'Bottom'
    
    IMAGE_CHOICES =[
        (top,'728 X 90px Top'),
        (side,'300 X 250px Side'),
        (bottom,'728 X 90px Bottom')
    ]

    name = models.CharField(max_length=200,choices=IMAGE_CHOICES,default=top)
    image = models.ImageField(null=True,blank=True)
    is_active = models.BooleanField(default=False)
    action_time = models.DateTimeField(auto_now=True)
    
    @property
    def image_tag(self):
        from django.utils.html import mark_safe
        return mark_safe('<img src="%s" width:"100px"  />'%(self.image.url))
        image_tag.short_description = 'Image'

    def __str__(self):
        return str(self.name) if self.name else ''

class Article(models.Model):
    author = models.ForeignKey(Author,on_delete=models.DO_NOTHING, null=True, related_name='author')
    header = models.ForeignKey(Header, on_delete=models.SET_NULL, null=True)
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    is_active = models.BooleanField(default=False)
    is_breaking_news = models.BooleanField(default=False)
    title = models.CharField(max_length=200)
    content = RichTextUploadingField(blank=True,null=True)
    
    def __str__(self):
       return str(self.title) if self.title else ''